﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectilescript : MonoBehaviour {
    public float power;
    public int timeout;
    private Rigidbody rb;

    void Start()
    {
        Invoke("die", timeout);
        rb = GetComponent<Rigidbody>();
        rb.AddRelativeForce(0,0,power);
    }
    void die()
    {
        Destroy(gameObject);
    }
}
