﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class shootscript : MonoBehaviour {

    public GameObject projectile;
    public float reloadtime;
    private bool reloading;

    void Update()
    {
        if (Input.GetKey(KeyCode.Mouse0) && reloading == false)
        {
            Instantiate(projectile, new Vector3(transform.position.x, transform.position.y, transform.position.z), transform.rotation);
            reloading = true;
            Invoke("reload", reloadtime);
        }
    }
    void reload()
    {
        reloading = false;
    }
}
