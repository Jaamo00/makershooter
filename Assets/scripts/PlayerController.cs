﻿using UnityEngine;
using UnityEngine.Networking;



public class PlayerController : NetworkBehaviour
{


    public GameObject bulletPrefab;
    public Transform bulletSpawn;
    public float power = 50;
    public float reloadtime;
    private bool reloading;



    void FixedUpdate()
    {
        Cursor.visible = false;

        if (!isLocalPlayer)
        {
            return;
        }
        
        var x = Input.GetAxis("Horizontal") * Time.deltaTime * 15.0f;
        var z = Input.GetAxis("Vertical") * Time.deltaTime * 15.0f;

        transform.Translate(x, 0, z);

        if (Input.GetButton("Fire1") && reloading == false)
        {
            CmdFire();
            reloading = true;
            Invoke("reload", reloadtime);

        }
    }

    [Command]
    void CmdFire()
    {

        // Create the Bullet from the Bullet Prefab

        var bullet = (GameObject)Instantiate(
          bulletPrefab,
          bulletSpawn.position,
          bulletSpawn.rotation);

        // Add velocity to the bullet

        bullet.GetComponent<Rigidbody>().velocity = bullet.transform.forward * power;

        bullet.GetComponent<Rigidbody>().velocity = bullet.transform.forward * 45;


        NetworkServer.Spawn(bullet);

        // Destroy the bullet after 2 seconds
        Destroy(bullet, 2.0f);
    }
    void reload()
    {
        reloading = false;
    }

    public override void OnStartLocalPlayer()
    {
        GetComponent<MeshRenderer>().material.color = Color.blue;
    }
}