﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class healthscript : MonoBehaviour {
    public int maxhp;
    public int hp;
    public float respawntimer;
    private Rigidbody rb;
    private MeshRenderer renderer;
    private Collider collider;
    private MonoBehaviour playerscript;
    private Transform gun;
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        playerscript = GetComponent<PlayerController>();
        renderer = GetComponent<MeshRenderer>();
        collider = GetComponent<CapsuleCollider>();
        gun = transform.Find("Camera/thompson");
    }
    void OnCollisionEnter(Collision collision)
    {
        if(collision.collider.tag == "Projectile")
        {
            hp--;
        }
    }
    void Update()
    {
        if(hp < 1 && collider.enabled == true)
        {
            Invoke("respawn", respawntimer);
            playerscript.enabled = false;
            renderer.enabled = false;
            collider.enabled = false;
            gun.gameObject.SetActive(false);
        }
        else if (hp < 1)
        {
            transform.Translate(0, 1, 0);
        }
    }
    void respawn()
    {
        transform.Translate(-1 * transform.position, Space.World);
        rb.velocity = new Vector3(0, 0, 0);
        hp = maxhp;
        playerscript.enabled = true;
        renderer.enabled = true;
        collider.enabled = true;
        gun.gameObject.SetActive(true);
    }
}
