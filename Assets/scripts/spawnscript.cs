﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spawnscript : MonoBehaviour {
    public int spawnxmin;
    public int spawnxmax;
    public int spawnymin;
    public int spawnymax;
    public int spawnzmin;
    public int spawnzmax;
    void Start () {
        transform.Translate(Random.Range(spawnxmin,spawnxmax), Random.Range(spawnymin, spawnymax), Random.Range(spawnzmin, spawnzmax));
	}
}
