﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class jumpscript : NetworkBehaviour {
    private bool jumpcooldown;
    public float jumpcooldowntime;
    public float jumpforce;
    public float grounddetectionrange;
    private Rigidbody rb;
	void Start () {
        rb = GetComponent<Rigidbody>();
	}

	void FixedUpdate () {
        if (!isLocalPlayer)
        {
            return;
        }
        if (Physics.Raycast(transform.position,Vector3.down, grounddetectionrange) == true && Input.GetKey(KeyCode.Space) && jumpcooldown == false)
        {
            rb.AddForce(Vector3.up * jumpforce);
            jumpcooldown = true;
            Invoke("cooldown", jumpcooldowntime);
        }
	}
    void cooldown()
    {
        jumpcooldown = false;
    }
}
