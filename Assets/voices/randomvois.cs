﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class randomvois : NetworkBehaviour
{
    
     AudioSource source;
    AudioSource oneshot;
    public AudioClip reload;
    public AudioClip walk;
    public AudioClip run;
    public AudioClip[] random;
    private AudioClip shootClip;


    

    void Start()
    {
        GameObject voice = transform.Find("voice").gameObject;

        source = voice.AddComponent<AudioSource>();
        oneshot = voice.AddComponent<AudioSource>();
    }
    void FixedUpdate()
    {
        if (!isLocalPlayer)
        {
            return;
        }
        if ((Input.GetAxis("Horizontal") !=0 || Input.GetAxis("Vertical") != 0))
        {
            if (Input.GetKey(KeyCode.LeftShift))
            {

                source.clip = run;

            }

            else

            {
                source.clip = walk;
            }
            if (!source.isPlaying )
            {
                source.Play();
            }
       }

        else
        {
            source.Stop();
        }
     


        if (Input.GetKeyDown(KeyCode.R))
            {
       
            oneshot.PlayOneShot(reload);
            }

        if (Input.GetKeyDown(KeyCode.B))
        {
            int index2 = Random.Range(0, random.Length);
            shootClip = random[index2];
            oneshot.PlayOneShot(shootClip);
        }

    }
    }